#include <string>
#include <map>
#include <set>

#include <spdlog/spdlog.h>
#include <ghc/filesystem.hpp>
#include <nlohmann/json.hpp>
#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <unicode/locid.h>

#include "utility.hpp"
#include "platform.hpp"
#include "rpgmakerVersion.hpp"
#include "foldertype.hpp"
#include "loggers.h"

using n_json = nlohmann::json;
namespace fs = ghc::filesystem;
using namespace std;


bool isValidDirectory(const string &directory, const string &name, const struct Loggers &loggers)
{
    if (!fs::exists(directory))
    {
        loggers.errorLogger->error("{} Folder does not exist!", name);
        return false;
    }

    if (!fs::is_directory(directory))
    {
        loggers.errorLogger->error("{} Folder is not a directory!", name);
        return false;
    }

    return true;
}

bool ensureDirectory(const fs::path &path, const struct Loggers &loggers)
{
    error_code ec;
    if (fs::exists(path, ec))
        return true;
    if (ec)
        return false;

    if (fs::create_directory(path, ec))
        return true;
    loggers.errorLogger->error("Unable to create directory {}! {}", path, ec);
    return false;
}

// trim from start (in place)
static inline void ltrim(string &s)
{
    s.erase(s.begin(), find_if(s.begin(), s.end(), [](unsigned char ch) {
                return !isspace(ch);
            }));
}

// trim from end (in place)
static inline void rtrim(string &s)
{
    s.erase(find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
                return !isspace(ch);
            }).base(),
            s.end());
}

// trim from both ends (in place)
static inline void trim(string &s)
{
    ltrim(s);
    rtrim(s);
}

bool getPlatforms(vector<string> *names, vector<Platform> *platforms, RPGMakerVersion version, const struct Loggers &loggers)
{
    for (auto &current : *names)
    {
        trim(current);
        if (current == "win")
        {
            platforms->emplace_back(Platform::Windows);
            continue;
        }

        if (current == "osx")
        {
            platforms->emplace_back(Platform::OSX);
            continue;
        }

        if (current == "linux")
        {
            if (version == RPGMakerVersion::MZ)
            {
                loggers.errorLogger->error("Linux is not supported in MZ!");
                return false;
            }
            platforms->emplace_back(Platform::Linux);
            continue;
        }

        if (current == "browser")
        {
            platforms->emplace_back(Platform::Browser);
            continue;
        }

        if (current == "joiplay")
        {
            platforms->emplace_back(Platform::JoiPlay);
            continue;
        }

        if (current == "mobile")
        {
            if (version == RPGMakerVersion::MV)
            {
                loggers.errorLogger->error("Mobile is not supported for MV at the moment!");
                return false;
            }
            platforms->emplace_back(Platform::Mobile);
            continue;
        }

        loggers.errorLogger->error("Unknown platform: {}", current);
        return false;
    }

    return true;
}

bool copyFile(const fs::path &from, const fs::path &to, bool useHardlinks, const struct Loggers &loggers, RPGMakerVersion version)
{
    auto target = to;
    auto filename = from.filename();
    auto extension = from.extension();
    auto parent = from.parent_path();
    auto parentName = parent.filename();
    auto grandparent = parent.parent_path();
    auto grandparentName = grandparent.filename();

    if (version == RPGMakerVersion::MV)
    {
        if (parentName == "system" && grandparentName == "img")
        {
            if (filename == "Loading.avif")
                target.replace_extension(".png");
            if (filename == "Window.avif")
                target.replace_extension(".png");
            if (filename == "Loading.webp")
                target.replace_extension(".png");
            if (filename == "Window.webp")
                target.replace_extension(".png");
        }
    }

    if (useHardlinks)
    {
        loggers.logger->debug("Creating hardlink from {} to {}", from, target);
    }
    else
    {
        loggers.logger->debug("Copying file from {} to {}", from, target);
    }

    error_code ec;
    auto options = fs::copy_options::none;
    if (useHardlinks)
    {
        options |= fs::copy_options::create_hard_links;
    }
    else
    {
        options |= fs::copy_options::update_existing;
    }

    if (useHardlinks)
    {
        if (fs::exists(target, ec))
        {
            if (fs::is_regular_file(target, ec))
            {
                auto result = fs::remove(target, ec);
                if (ec || !result)
                {
                    loggers.errorLogger->error("Unable to delete file for hardlink at {}! {}", target, ec);
                    return false;
                }
            }
        }
    }

    fs::copy(from, target, options, ec);
    if (ec)
    {
        if (useHardlinks)
            loggers.errorLogger->error("Unable to create hardlink from {} to {}! {}", from, target, ec);
        else
            loggers.errorLogger->error("Unable to copy file from {} to {}! {}", from, target, ec);
        return false;
    }

    if (!useHardlinks)
    {
        setPermissionsAndTimestamp(from, target);
    }

    return true;
}

unsigned int *stringToHexHash(string encryptionHash)
{
    auto result = new unsigned int[16];
    if (encryptionHash.empty())
        return result;

    for (auto i = 0, j = 0; i < 32; i += 2, j++)
    {
        auto c1 = encryptionHash[i];
        auto c2 = encryptionHash[i + 1];

        auto c = string("");
        c.push_back(c1);
        c.push_back(c2);

        auto x = stoul(c, nullptr, 16);
        result[j] = x;
    }

    return result;
}

auto lastPlatform = Platform::None;
map<wstring, wstring> cachedEncryptedFiles;

static uint8_t header[] = {0x52, 0x50, 0x47, 0x4D, 0x56, 0x00, 0x00, 0x00, 0x00, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};

bool isImage(fs::path *target)
{
    return (target->extension() == ".png" ||
            target->extension() == ".webp" ||
            target->extension() == ".avif");
}

bool encryptFile(const fs::path &from, fs::path to, const unsigned int *encryptionHash, bool useCache, bool hardlink, Platform platform, RPGMakerVersion version, const struct Loggers &loggers)
{
    auto file = from;
    auto extension = from.extension();

    if (extension == ".ogg")
    {
        if (version == RPGMakerVersion::MV)
            to.replace_extension(".rpgmvo");
        else
            to.replace_extension(".ogg_");
    }
    else if (extension == ".m4a")
    {
        if (version == RPGMakerVersion::MV)
            to.replace_extension(".rpgmvm");
        else
            to.replace_extension(".m4a_");
    }
    else if (isImage(&file) )
    {
        if (version == RPGMakerVersion::MV)
            to.replace_extension(".rpgmvp");
        else
            to.replace_extension(".png_");
    }
    else
    {
        loggers.errorLogger->error("Unknown extension {} of file {}", extension, from);
        return false;
    }

    if (useCache)
    {
        if (lastPlatform == Platform::None)
            lastPlatform = platform;
        else if (lastPlatform != platform)
        {
            //hardlink to the previously encrypted files
            if (hardlink)
            {
                auto iter = cachedEncryptedFiles.find(from.wstring());
                if (iter != cachedEncryptedFiles.end())
                {
                    auto prev = fs::path(iter->second);

                    error_code ec;
                    if (fs::exists(prev, ec))
                    {
                        loggers.logger->debug("Using cached path from encryption file at {} and hardlink to {}", prev, to);
                        if (copyFile(prev, to, true, loggers, version))
                            return true;
                        return false;
                    }
                    else
                    {
                        loggers.logger->warn("Cached path {} does not exist anymore!", prev);
                        cachedEncryptedFiles.erase(iter);
                    }
                }
                else
                {
                    loggers.logger->warn("Unable to find path {} in cached map!", from);
                }
            }
        }
    }

    loggers.logger->debug("Encoding file at {} to {}", from, to);

    auto ifstream = fs::ifstream(from, ios_base::in | ios_base::binary);
    if (!ifstream.is_open())
    {
        loggers.errorLogger->error("Unable to open file {}", from);
        return false;
    }

    auto ofstream = fs::ofstream(to, ios_base::out | ios_base::binary);
    if (!ofstream.is_open())
    {
        loggers.errorLogger->error("Unable to open file {}", to);
        ifstream.close();
        return false;
    }

    ifstream.seekg(0, ios_base::end);
    auto fileLength = ifstream.tellg();
    ifstream.seekg(0, ios_base::beg);

    if (fileLength < 16)
    {
        loggers.errorLogger->error("File {} is less than 16 bytes long: {}", from, fileLength);
        return false;
    }

    /*
        "Encryption" in RPG Maker:
        1) write a header
        2) encrypt the first 16 bytes from the original file using a simple XOR operation
        3) write the rest of the original file
    */

    ofstream.write((char *)&header, 16);

    auto bytes = new char[16];
    ifstream.read(bytes, 16);

    for (auto i = 0; i < 16; i++)
    {
        auto c1 = bytes[i];
        auto c2 = encryptionHash[i];
        auto result = c1 ^ c2;
        bytes[i] = (char)result;
    }
    ofstream.write(bytes, 16);

    auto buffer = new char[4096];
    auto max = (long)fileLength - 4096;
    while (ifstream.tellg() < max)
    {
        ifstream.read(buffer, 4096);
        ofstream.write(buffer, 4096);
    }

    if (ifstream.tellg() != fileLength)
    {
        auto left = (long)(fileLength - ifstream.tellg());
        ifstream.read(buffer, left);
        ofstream.write(buffer, left);
    }

    ifstream.close();
    ofstream.close();
    delete[] bytes;
    delete[] buffer;

    setPermissionsAndTimestamp(from, to);

    cachedEncryptedFiles[from.wstring()] = to.wstring();

    return true;
}

bool compressJson(const fs::path &from, const fs::path &to, const struct Loggers &loggers)
{
    loggers.logger->debug("Removing whitespace: from {} to {}", from, to);

    auto ifstream = fs::ifstream(from, ios_base::in);
    if (!ifstream.is_open())
    {
        loggers.errorLogger->error("Unable to open file {}", from);
        return false;
    }
    n_json systemJson = n_json::parse(ifstream);
    ifstream.close();

    auto ofstream = fs::ofstream(to, ios_base::out);
    if (!ofstream.is_open())
    {
        loggers.errorLogger->error("Unable to open file {}", to);
        ifstream.close();
        return false;
    }
    ofstream << systemJson;
    ofstream.close();

    setPermissionsAndTimestamp(from, to);
    return true;
}

bool updateSystemJson(const fs::path &from, const fs::path &to, bool encryptAudio, bool encryptImages, const string &hash, const struct Loggers &loggers)
{
    loggers.logger->debug("Updating System.json with encryption data from {} to {}", from, to);

    auto ifstream = fs::ifstream(from, ios_base::in);
    if (!ifstream.is_open())
    {
        loggers.errorLogger->error("Unable to open file {}", from);
        return false;
    }
    n_json systemJson = n_json::parse(ifstream);
    ifstream.close();
    systemJson["hasEncryptedImages"] = encryptImages;
    systemJson["hasEncryptedAudio"] = encryptAudio;
    systemJson["encryptionKey"] = hash;

    auto ofstream = fs::ofstream(to, ios_base::out);
    if (!ofstream.is_open())
    {
        loggers.errorLogger->error("Unable to open file {}", to);
        ifstream.close();
        return false;
    }
    ofstream << systemJson;

    ofstream.close();

    setPermissionsAndTimestamp(from, to);

    return true;
}

RPGMakerVersion getRPGMakerVersion(const fs::path &projectPath, const struct Loggers &loggers)
{
    loggers.logger->debug("Identifying RPGMaker Version");

    error_code ec;
    auto version = RPGMakerVersion::None;
    for (const auto &p : fs::directory_iterator(projectPath, FOLDER_SEARCH_OPTIONS, ec))
    {
        if (!p.is_regular_file())
            continue;
        auto extension = p.path().extension();

        if (extension == ".rpgproject")
        {
            version = RPGMakerVersion::MV;
            break;
        }
        else if (extension == ".rmmzproject")
        {
            version = RPGMakerVersion::MZ;
            break;
        }
    }

    if (version == RPGMakerVersion::None)
    {
        loggers.errorLogger->error("Unable to identify RPGMaker Version!");
    }
    else
    {
        loggers.logger->info("RPGMaker Version: {}", version);
    }

    return version;
}

/***
 * Case insensitive matching
 */
bool matchNC(string left, string right)
{
    icu::UnicodeString item1(left.c_str(), "UTF-8");
    icu::UnicodeString item2(right.c_str(), "UTF-8");
    return item1 == item2;
}

#define IGNORE_NWJS_FILE(x) if(matchNC(filename, x)) return true
#define IGNORE_NWJS_FOLDER(x) if(matchNC(parentName, x)) return true

bool filterFile(fs::path *from, fs::path *to, FolderType folderType, RPGMakerVersion version, Platform platform)
{
    auto filename = from->filename();
    auto extension = from->extension();

    auto parent = from->parent_path();
    auto parentName = parent.filename();

    if (folderType == FolderType::RPGMaker)
    {
        // TODO: Fix OSX MV to work with SDK target
        if (platform == Platform::OSX)
        {
            if (version == RPGMakerVersion::MZ)
            {
                if (filename == "chromedriver")
                    return true;
                if (filename == "minidump_stackwalk")
                    return true;
                if (filename == "nwjc")
                    return true;
                if (filename == "payload")
                    return true;

                if (filename == "v8_context_snapshot.bin" && parentName != "Resources")
                    return true;
                if (filename == "libffmpeg.dylib" && parentName != "80.0.3987.149")
                    return true;
            }
        } else {
            if (matchNC(filename, "nw.exe"))
            {
                to->replace_filename("Game.exe");
                return false;
            }
            if (matchNC(filename, "nw"))
            {
                to->replace_filename("Game");
                return false;
            }
            IGNORE_NWJS_FOLDER("pnacl");
            IGNORE_NWJS_FILE("chromedriver.exe");
            IGNORE_NWJS_FILE("nacl_irt_x86_64.nexe");
            IGNORE_NWJS_FILE("nwjc.exe");
            IGNORE_NWJS_FILE("payload.exe");
            IGNORE_NWJS_FILE("chromedriver");
            IGNORE_NWJS_FILE("crashpad_handler");
            IGNORE_NWJS_FILE("nacl_helper");
            IGNORE_NWJS_FILE("nacl_helper_bootstrap");
            IGNORE_NWJS_FILE("nwjc");
            IGNORE_NWJS_FILE("payload");
        }
    }
    else if (folderType == FolderType::Project)
    {
        // Desktop: only ogg
        // Mobile: only m4a
        // Browser: both
        if (platform == Platform::Mobile && extension == ".ogg")
            return true;
        if (extension == ".m4a" && platform != Platform::Mobile && platform != Platform::Browser)
            return true;

        // skip project files
        if (extension == ".rpgproject")
            return true;
        if (extension == ".rmmzproject")
            return true;

        // skip saves
        if (extension == ".rpgsave")
            return true;
        if (extension == ".rmmzsave")
            return true;
        if (parentName == "save")
            return true;
    }

    return false;
}

bool isAudio(fs::path *target)
{
    return (target->extension() == ".ogg" || target->extension() == ".m4a");
}

bool shouldEncryptFile(fs::path *from, bool encryptAudio, bool encryptImages, RPGMakerVersion version)
{
    auto filename = from->filename();
    auto extension = from->extension();
    auto parent = from->parent_path();
    auto parentName = parent.filename();
    auto grandparent = parent.parent_path();
    auto grandparentName = grandparent.filename();

    if (!isImage(from) && !isAudio(from))
        return false;

    if (encryptAudio && isAudio(from))
    {
        return true;
    }

    if (encryptImages && isImage(from))
    {
        if (version == RPGMakerVersion::MZ)
        {
            //effects/Textures is not encrypted in MZ
            if (parentName == "Texture" && grandparentName == "effects")
                return false;
        }
        else if (version == RPGMakerVersion::MV)
        {
            if (parentName == "system" && grandparentName == "img")
            {
                //these are for some reason not encrypted in MV
                if (filename == "Loading.png")
                    return false;
                if (filename == "Window.png")
                    return false;
                if (filename == "Loading.webp")
                    return false;
                if (filename == "Window.webp")
                    return false;
                if (filename == "Loading.avif")
                    return false;
                if (filename == "Window.avif")
                    return false;
            }
        }

        //game icon for the window
        if (filename == "icon.png" && parentName == "icon")
            return false;

        return true;
    }

    return false;
}

#define FIND(path, set)         \
    else if (parent == path)    \
    {                           \
        it = set.find(name);    \
        return it == set.end(); \
    }

bool filterUnusedFiles(const fs::path &path, struct InputPaths *inputPaths, struct ParsedData *parsedData, RPGMakerVersion version)
{
    auto filename = path.filename().u8string();
    auto extension = path.extension().u8string();
    auto parent = path.parent_path();

    auto name = filename.substr(0, filename.find(extension));

    set<string>::iterator it;

    if (parent == inputPaths->bgmPath)
    {
        it = parsedData->bgmNames.find(name);
        return it == parsedData->bgmNames.end();
    }
    else if (parent == inputPaths->enemiesFrontViewBattlerPath || parent == inputPaths->enemiesSideViewBattlerPath)
    {
        it = parsedData->enemyBattlerNames.find(name);
        return it == parsedData->enemyBattlerNames.end();
    }
    FIND(inputPaths->bgsPath, parsedData->bgsNames)
    FIND(inputPaths->mePath, parsedData->meNames)
    FIND(inputPaths->sePath, parsedData->seNames)
    FIND(inputPaths->moviesPath, parsedData->movieNames)
    FIND(inputPaths->picturesPath, parsedData->pictureNames)
    FIND(inputPaths->titles1Path, parsedData->title1Names)
    FIND(inputPaths->titles2Path, parsedData->title2Names)
    FIND(inputPaths->charactersPath, parsedData->characterNames)
    FIND(inputPaths->facesPath, parsedData->faceNames)
    FIND(inputPaths->actorsBattlerPath, parsedData->actorBattlerNames)
    FIND(inputPaths->tilesetsPath, parsedData->tilesetNames)
    FIND(inputPaths->battlebacks1Path, parsedData->battleback1Names)
    FIND(inputPaths->battlebacks2Path, parsedData->battleback2Names)
    FIND(inputPaths->parallaxesPath, parsedData->parallaxNames)

    //MV only:
    FIND(inputPaths->animationsPath, parsedData->animationNames)
    //MZ only:
    FIND(inputPaths->effectsPath, parsedData->effectNames)

    auto pathName = path.u8string();
    //check if version is MZ because effectsPath will be L"" on MV and some files will be excluded that should not be
    if (version == RPGMakerVersion::MZ && pathName.find(inputPaths->effectsPath.u8string()) != string::npos)
    {
        auto iterator = parsedData->effectResources.find(pathName);
        return iterator == parsedData->effectResources.end();
    }

    return false;
}

#define GET_PLATFORM(p, n) \
    if (platform == p)     \
    {                      \
        return n;          \
    }

string getPlatformFolder(RPGMakerVersion version, Platform platform)
{
    if (version == RPGMakerVersion::MV)
    {
        GET_PLATFORM(Platform::Windows, "nwjs-win")
        GET_PLATFORM(Platform::OSX, "nwjs-osx-unsigned")
        GET_PLATFORM(Platform::Linux, "nwjs-lnx")
        GET_PLATFORM(Platform::Browser, "")
        GET_PLATFORM(Platform::Mobile, "")
    }
    else
    {
        GET_PLATFORM(Platform::Windows, "nwjs-win")
        GET_PLATFORM(Platform::OSX, "nwjs-mac")
        GET_PLATFORM(Platform::Linux, "")
        GET_PLATFORM(Platform::Browser, "")
        GET_PLATFORM(Platform::Mobile, "")
    }

    return "";
}

void setPermissionsAndTimestamp(fs::path from, fs::path to)
{
    // Copy modified time and permissions
    // Permissions must be modified first
    fs::permissions(to, fs::status(from).permissions(), fs::perm_options::replace);
    fs::last_write_time(to, fs::last_write_time(from));
}
