#pragma once

#include <string>
#include <set>

#include <spdlog/spdlog.h>
#include <ghc/filesystem.hpp>
#include <simdjson.h>

#include "rpgmakerVersion.hpp"

using namespace std;
namespace fs = ghc::filesystem;

struct ParsedData {
    //Actors.json
    //img/sv_actors/{}.png
    set<string> actorBattlerNames;

    //Animations.json
    set<uint64_t> animationIds;
    //MV only: img/animations/{}.png
    set<string> animationNames;
    //MZ only: effects/{}.efkefc
    set<string> effectNames;
    //MZ only: effects/{}
    set<string> effectResources;

    //Enemies.json
    //img/enemies/{}.png
    set<string> enemyBattlerNames;

    //Tilesets.json
    //img/tilesets/{}.png+{}.txt
    set<string> tilesetNames;

    //System.json
    //img/titles1/{}.png
    set<string> title1Names;
    //img/titles2/{}.png
    set<string> title2Names;

    //other
    //img/characters/{}.png
    set<string> characterNames;
    //img/faces/{}.png
    set<string> faceNames;

    //audio/bgm/{}.m4a/ogg
    set<string> bgmNames;
    //audio/bgs/{}.m4a/ogg
    set<string> bgsNames;
    //audio/me/{}.m4a/ogg
    set<string> meNames;
    //audio/se/{}.m4a/ogg
    set<string> seNames;

    //img/pictures/{}.png
    set<string> pictureNames;
    //movies/{}.webm
    set<string> movieNames;

    //img/battlebacks1/{}.png
    set<string> battleback1Names;
    //img/battlebacks2/{}.png
    set<string> battleback2Names;
    //img/parallaxes/{}.png
    set<string> parallaxNames;
};

bool parseData(const fs::path& dataFolder, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseEvents(simdjson::dom::array& eventList, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);

bool parseActors(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseAnimations(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseCommonEvents(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseEnemies(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseItems(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseMap(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseSkills(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseSystem(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseTilesets(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseTroops(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);
bool parseWeapons(const fs::path& path, struct ParsedData* parsedData, RPGMakerVersion rpgMakerVersion, const struct Loggers& loggers);

struct EffectInfoChunk {
    uint32_t name;
    uint32_t size;
    uint32_t unknown;
};

struct EffectHeader {
    uint32_t magic;
    uint32_t version;
};

bool parseEffect(const fs::path& path, const fs::path& effectsPath, struct ParsedData* parsedData, const struct Loggers& loggers);
