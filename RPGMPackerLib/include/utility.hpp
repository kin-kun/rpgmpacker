#pragma once

#include <string>

#include <spdlog/spdlog.h>
#include <ghc/filesystem.hpp>

#include "formatters.hpp"
#include "loggers.h"
#include "foldertype.hpp"
#include "platform.hpp"
#include "rpgmakerVersion.hpp"
#include "parseData.hpp"
#include "inputPaths.hpp"

using namespace std;
namespace fs = ghc::filesystem;

#define FOLDER_SEARCH_OPTIONS (ghc::filesystem::directory_options::follow_directory_symlink | ghc::filesystem::directory_options::skip_permission_denied)

bool isValidDirectory(const string& directory, const string& name, const struct Loggers& loggers);
bool ensureDirectory(const fs::path& path, const struct Loggers& loggers);
bool getPlatforms(vector<string>* names, vector<Platform>* platforms, RPGMakerVersion version, const struct Loggers& loggers);
bool copyFile(const fs::path& from, const fs::path& to, bool useHardlinks, const struct Loggers& loggers, RPGMakerVersion version);
unsigned int* stringToHexHash(string encryptionHash);
bool encryptFile(const fs::path& from, fs::path to, const unsigned int* encryptionHash, bool useCache, bool hardlink, Platform platform, RPGMakerVersion version, const struct Loggers& loggers);
bool updateSystemJson(const fs::path& from, const fs::path& to, bool encryptAudio, bool encryptImages, const string& hash, const struct Loggers& loggers);
RPGMakerVersion getRPGMakerVersion(const fs::path& projectPath, const struct Loggers& loggers);
bool compressJson(const fs::path& from, const fs::path& to, const struct Loggers& loggers);
bool filterFile(fs::path* from, fs::path* to, FolderType folderType, RPGMakerVersion version, Platform platform);
bool shouldEncryptFile(fs::path *from, bool encryptAudio, bool encryptImages, RPGMakerVersion version);
bool filterUnusedFiles(const fs::path& path, struct InputPaths* inputPaths, struct ParsedData* parsedData, RPGMakerVersion version);
string getPlatformFolder(RPGMakerVersion version, Platform platform);
void setPermissionsAndTimestamp(fs::path from, fs::path to);
bool matchNC(string left, string right);
