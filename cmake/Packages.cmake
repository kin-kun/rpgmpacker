CPMAddPackage ("gh:jarro2783/cxxopts@2.2.1")
CPMAddPackage ("gh:gabime/spdlog@1.8.2")
CPMAddPackage ("gh:taskflow/taskflow@3.0.0")
CPMAddPackage ("gh:ToruNiina/toml11@3.6.0")

CPMAddPackage (
        NAME ghc_filesystem
        GITHUB_REPOSITORY gulrak/filesystem
        VERSION 1.5.2
        EXCLUDE_FROM_ALL YES
        OPTIONS
        "GHC_FILESYSTEM_ENFORCE_CPP17_API On"
)

CPMAddPackage (
        NAME simdjson
        GITHUB_REPOSITORY simdjson/simdjson
        VERSION 0.8.1
        OPTIONS
        "SIMDJSON_JUST_LIBRARY On"
        "SIMDJSON_BUILD_STATIC On"
)

CPMAddPackage ("gh:ToruNiina/toml11@3.6.0")

CPMAddPackage(
        NAME nlohmann_json
        VERSION 3.9.1
        # the git repo is incredibly large, so we download the archived include directory
        URL https://github.com/nlohmann/json/releases/download/v3.9.1/include.zip
        URL_HASH SHA256=6bea5877b1541d353bd77bdfbdb2696333ae5ed8f9e8cc22df657192218cad91
)

if (nlohmann_json_ADDED)
        add_library(nlohmann_json INTERFACE IMPORTED)
endif()

CPMAddPackage("gh:onqtam/doctest#2.4.5")
